(function ($) {
  'use strict';

  window.Drupal.behaviors.disqusBubble = {
    attach: function (context, settings) {
      var button = $('.comment-count');

      button.each(function (index, item) {
        $(item).append($('<span/>', {
          'class': 'disqus-comment-count', 'data-disqus-identifier': settings.disqusBubble.identifier, text: ' '
        }));
      });

      $.ajax({
        type: 'GET', url: '//' + settings.disqusBubble.domain + '.disqus.com/count.js', dataType: 'script', cache: false
      }).done(function () {
        button.find('span.disqus-comment-count').each(function (index, item) {
          var handle = setInterval(function () {
            var current = $(item);
            var text = current.text();

            if (' ' === text) {
              return;
            }

            clearInterval(handle);
            var count = parseInt(text.split(' ').shift(), 10);

            if (0 < count) {
              current.text(count);
              current.parent().removeClass('empty');
              current.show();
            } else {
              current.hide();
            }
          }, 300);
        });
      });
    }
  };
})(jQuery);
